﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestAssemblyExtensions.TestData
{
    public class SimpleObjectWithParent : Parent
    {
        public int Field1;
        public List<string> X { get; set; }
        private int Z = 10;

        public int GetCount()
        {
            return Z;
        }
    }
    public class Parent : Grandparent
    {
        public int ParentProperty { get; set; }
    }
    public class Grandparent
    {
        public int GrandparentProperty { get; set; }
    }
}

namespace TestAssemblyExtensions.TestData1
{
    public abstract class AbsClass
    {
        public int X { get; set; }
    }

    public interface IInterfaceClass
    {
        void X();
        int Ix { get; set; }
    }

    public class InterfaceClass : IInterfaceClass
    {
        public void X()
        {
            throw new NotImplementedException();
        }

        public int Ix { get; set; }
    }
}