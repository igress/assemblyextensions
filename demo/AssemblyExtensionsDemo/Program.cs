﻿using System.Reflection;
using AssemblyExtensionsLib.Extensions;

namespace AssemblyExtensionsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var assembly = typeof(Program).GetTypeInfo().Assembly;

            var typeNames = assembly.GetAllTypesNames();

            var types = assembly.GetAllTypes();

            var members = assembly.GetAllMemberNames();

            var namespaces = assembly.GetAllNameSpacesNames();

            var allMethods = assembly.GetAllMethods();

            var allMethodsMeta = assembly.GetAllMethodsMetaData();
        }
    }
}