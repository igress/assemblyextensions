﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using PrintClassInstanceLib.Extensions;
using PrintClassInstanceLib.Model;

namespace AssemblyExtensionsLib.Extensions
{
    public static class ExtensionMethods
    {
        public static List<Type> GetAllTypes(this Assembly assembly)
        {
            try
            {
                return assembly.DefinedTypes.ToList()
                        .Select(s => s.AsType())
                        .Where(s => s != null)
                        .ToList()
                        .OrderBy(s => s.Name)
                        .ToList();
            }
            catch (ReflectionTypeLoadException ex)
            {
                return  ex.Types.Where(t=>t!=null).ToList();
            }
            catch (Exception ex)
            {
                return new List<Type>();
            }
        }

        public static Dictionary<string, List<string>> GetAllMemberNames(this Assembly assembly)
        {
            var types = assembly.GetAllTypes();
            var memberNames = new Dictionary<string, List<string>>();
            foreach (var type in types)
            {
                var ns = $"{type.GetNamespace()}-{type.Name}";
                var names = type.GetAllMemberNames();
                memberNames[ns] = names;
            }
            return memberNames;
        }

        public static List<string> GetAllNameSpacesNames(this Assembly assembly)
        {
            var nameSpaces = assembly.GetAllTypes().Select(type => type.GetNamespace()).ToList();
            return !nameSpaces.Any() ? nameSpaces : nameSpaces.Distinct().OrderBy(s => s).ToList();
        }

        public static List<string> GetAllTypesNames(this Assembly assembly)
        {
            return assembly.GetAllTypes().Select(s => s.Name).ToList();
        }

        public static List<MethodInfo> GetAllMethods(this Assembly assembly)
        {
            return assembly.GetAllTypes().SelectMany(s => s.GetAllMethods() ).ToList();
        }

        public static List<MethodMeta> GetAllMethodsMetaData(this Assembly assembly)
        {
            return assembly.GetAllTypes().SelectMany(s => s.GetAllMethodsMetaData()).ToList();
        }
    }
}
